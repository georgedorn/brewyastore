# BrewyaOnOuya

This repo will help you get a local Brewya storefront up and running that will allow your OUYA to host its very own login server.

## Prerequisities

1. Basic understanding of a command line
1. An OUYA!

## Getting the files and environment ready

1. Download this repo to your computer
1. Get ADB installed on your computer
1. ADB can be run over the microUSB or Network.
    1. If using USB:
        1. Connect the OUYA to your computer via USB now
    1. If using network:
        * *Note:* We couldn't get ADB over network to run when only using wifi
        1. Plug in your OUYA to ethernet
        1. On your OUYA, go to *Manage* -> *System* -> *Development* and enable "*ADB over network*"
        1. On your computer, type `adb connect x.x.x.x:5555` where *x.x.x.x* is the IP address currently displayed on your OUYA
            1. You can have multiple adb connections to the same host when using ADB over network if that matters to you for any reason (does not affect anything in this guide)
            * *Note*: On windows, it can be ./adb.exe or just adb.exe depending on your environment (powershell, cmd) and will apply to the rest of this document
1. You can verify that you're ready to move on by running the command `adb devices` and having it return the line `List of devices attached` followed on the next line with a set of hexadecimal characters or the OUYA IPaddress + port all with the word "device" to the right

## Installing and configuring stuff

1.  Run `cd path/to/brewyastore`
1.  Run `adb install files/qpython-app-release.apk`
1.  Run `adb push . /sdcard/tmp`
1.  Run `adb shell`
1.  Now that you're in the shell, execute these commands:
    1.  Run `su`
    1.  Run `cd /sdcard/tmp/brewyastore`
    1.  Run `source files/qpyenv.sh`
    1.  Run `python files/get-pip.py`
        1. *Note*: You can use the latest [`get-pip.py`](https://bootstrap.pypa.io/get-pip.py), but you'll need to add this line after all of the `import` statements: `sys.getfilesystemencoding = lambda: 'UTF-8'`
    1.  Edit `/data/data/org.qpython.qpy/files/lib/python2.7/site-packages/pip/__init__.py`
        1. `vi` is the only editor I'm aware of. Run `vi /data/data/org/...` to open the file for editing
        1. Use the keboard shortcut `i` to be able to insert/edit data
        1. Add this line after all of the `import` statements: `sys.getfilesystemencoding = lambda: 'UTF-8'`
        1. After you've edited the file, press `Escape` to exit the insert mode and then type `:x` to save and close (or just `:q!` if you need to exit without saving changes)
    1.  Run `pip install -r requirements.txt`

## Run the server and test
1. Run `cp /sdcard/tmp/OUYA/ouya_config.properties /sdcard/`
1. Run `invoke start`
    1.  If you're running the server on a device other than the OUYA, bind it to an external network adapter, with `invoke start --external`
    1.  You will also need to edit the contents of `ouya_config.properties` accordingly
1. From OUYA on your TV: Logout of your OUYA account, if needed
1. Login using bogus credentials
1. Disable "*ADB over network*" if you were using it
1. Fin

## Troubleshooting

* If you get an error about "need string instead of null" or something, you did not get the `__init__.py` file edited correctly

## Returning to the main OUYA store

The `ouya_config.properties` file is queried every time the OUYA is going to connect to a server. If you move this file to have a different name, your OUYA will immediately begin using the default url (devs.ouya.tv) again.
1. From `adb shell`, run `mv /sdcard/ouya_config.properties /sdcard/ouya_config.properties.inactive`
    * Ooptionally: Use `vi` to edit `/sdcard/ouya_config.properties` and change the server from `localhost:8000` to `devs.ouya.tv`
        * *Warning*: Be sure not to add a trailing `/` to the above URL
        * Comments work in this file (lines beginning with `#`), so you can have two lines for OUYA_SERVER_URL and toggle between which one is inactive with `#` in front of the inactive line. Example:
            * `OUYA_SERVER_URL=http://localhost:8000`
            * `#OUYA_SERVER_URL=http://SomeOtherServer:8000`
1. If you want to fully back out all changes, delete the `/sdcard/tmp` folder and uninstall the qpython app

## Next steps

1. Autorun on boot?
