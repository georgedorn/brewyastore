from rest_framework import routers


API = routers.DefaultRouter()


def register(route_name):
    """Automatically adds viewsets to a `rest_framework.DefaultRouter`"""
    def _inner(viewset):
        API.register(route_name, viewset)
        return viewset
    return _inner