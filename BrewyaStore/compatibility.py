import sys

PY3 = sys.version_info[0] == 3

items = lambda d, **kw: d.iteritems(**kw)
if PY3:
    items = lambda d, **kw: iter(d.items(**kw))