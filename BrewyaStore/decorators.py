import os
from functools import wraps

from django.conf.urls import url
from django.http import Http404
from rest_framework import serializers, viewsets

# This is a strange one.  `memoprop` needs to live in utils so that settings 
# can import it, but it's a decorator that should be imported from here
from brewyastore.settings_helpers import memoprop
from brewyastore.routers import register


def methods(*allowed_methods):
    """If the request method is one of the supplied allowed methods, call proceeds.  Otherwise 404 is raised"""
    def _outer(f):
        if not allowed_methods:
            return f

        @wraps(f)
        def _inner(request):
            if request.method in allowed_methods:
                return f(request)
            raise Http404
        return _inner
    return _outer
_methods = methods


URLPATTERNS = []
def route(location, methods=()):
    """
    Flask-style view routing in Django!  Just decorate your view function with this and give it a route

    urlpatterns automatically get updated with anything registered by this mechanism
    """
    def _inner(f):
        f = _methods(*methods)(f)
        URLPATTERNS.append(url(location + '$', f, name=f.__name__))
        return f
    return _inner


def endpoint(endpoint_name):
    """Takes a Django model, and dynamically generates a `rest_framework.ModelSerializer` and a `rest_framework.ModelViewSet` to go along with it"""
    def _inner(model_class):
        Meta = type('Meta', (), {'model': model_class, 'fields': '__all__'})
        model_class.serializer = type(model_class.__name__ + 'Serializer', (serializers.ModelSerializer,), {'Meta': Meta})

        local_vars = {'queryset': model_class.objects.all(), 'serializer_class': model_class.serializer}
        model_class.viewset = register(endpoint_name)(type(model_class.__name__ + 'ViewSet', (viewsets.ModelViewSet,), local_vars))
        
        return model_class
    return _inner

