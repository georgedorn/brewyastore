from brewyastore.settings_helpers import IPV4Range

# TODO: Write lots more unit tests

class TestIPV4Range:
    def test_static_ip(self):
        ip_range = IPV4Range('192.168.1.243')
        assert '192.168.1.243' in ip_range
        assert '173.246.8.104' not in ip_range

    def test_cidr_subnet(self):
        ip_range = IPV4Range('192.168.0.0/16')
        assert '192.168.100.10' in ip_range
        assert '192.168.0.1' in ip_range
        assert '10.0.0.1' not in ip_range
        assert '192.167.100.10' not in ip_range

    def test_cidr_only_notation(self):
        ip_range = IPV4Range('/2')
        assert ip_range.bitmask is not None
        assert '192.168.100.10' in ip_range
        assert '192.168.0.1' in ip_range
        assert '10.0.0.1' not in ip_range
        # import pudb; pu.db
        assert '192.167.100.10' in ip_range

    def test_wildcards(self):
        ip_range = IPV4Range('192.168.*.*')
        assert '192.168.100.10' in ip_range
        assert '192.168.0.1' in ip_range
        assert '10.0.0.1' not in ip_range
        assert '192.167.100.10' not in ip_range

    def test_single_wildcard(self):
        ip_range = IPV4Range('*')
        assert '192.168.100.10' in ip_range
        assert '192.168.0.1' in ip_range
        assert '10.0.0.1' in ip_range
        assert '192.167.100.10' in ip_range
