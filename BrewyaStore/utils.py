import os

from django.conf.urls import url
from django.http import FileResponse

from brewyastore.compatibility import items


def simple_json_response(endpoint):
    """Dynamically generates view functions that load and return a response from a JSON file on disk"""
    from django.conf import settings

    def _inner(request):
        filename = os.path.join(settings.BASE_DIR, 'store', 'responses', '{}.json'.format(endpoint))
        return FileResponse(open(filename, 'rb'), content_type='application/json')
    return _inner


def json_urls(responses):
    """Dynamically generates urlpatterns mapping to a collection of JSON responses"""
    return [url(route + '$', simple_json_response(endpoint), name=endpoint) for route, endpoint in items(responses)]
