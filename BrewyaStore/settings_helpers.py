from functools import wraps
import os

from django.core.management.utils import get_random_secret_key


def memoprop(func):
    """
    Creates a memoized property out of a method

    Memoization caches the result of calling a function, and uses the cache to supply the same result later
    A property is a method that acts like an attribute, where you don't have to "call" it with parenthesis.

    Since a property takes no arguments, a memoization cache only needs to hold one value.

    The value of this is that you don't have to initialize values you aren't going to need, nor keep track of
    what has been initialized.  Just supply the formula for calculating some expensive value in a function,
    access it when you need it, and this decorator will keep everything efficient.
    """
    @wraps(func)
    def _inner(self):
        var_name = '_{}'.format(func.__name__)
        if not hasattr(self, var_name):
            setattr(self, var_name, func(self))

        return getattr(self, var_name)
    return property(_inner)


class IPV4Range:
    """
    Specify a range of IPV4 addressess with one of the following forms:
      * Static -- 192.168.1.1
      * Wildcard -- 192.168.*.10
      * CIDR block extension -- 192.168.1.1/24
      * CIDR block level -- /2  (Has two leading bits, so would look like 192.0.0.0/2)

    Includes method supporting the `in` keyword, allowing you to test if a single discreet IPV4 address falls within the initialized range
    """
    def __init__(self, address):
        self.address = address

        addr, _, block_level = address.strip().partition('/')

        if '*' in addr and block_level:
            raise Exception('Cannot use wildcards with CIDR block range notation')

        if addr == '*':
            addr = '*.*.*.*'

        self.octets = list(map(lambda c: c if c == '*' else int(c), addr.split('.'))) if addr else None
        self.block_level = int(block_level) if block_level else None

    def __repr__(self):
        return '{}({!r})'.format(self.__class__.__name__, self.address)

    def __contains__(self, address):
        """
        This is the magic method that makes the `in` keyword work.
        
        Use it like this:
        
        ```
        >>> ipv4_range = IPV4Range('192.168.0.0/16')
        >>> '192.168.1.132' in ipv4_range
        True
        >>> '173.56.72.138' in ipv4_range
        False
        ```
        """
        octets = map(int, address.strip().split('.'))
        if self.block_level is not None:
            return not ((self.to_number(octets) ^ self.bitstring) & self.bitmask)

        return all(o2 == '*' or o1 == o2 for o1, o2 in zip(octets, self.octets))

    @memoprop
    def bitmask(self):
        """
        Generates the bitmask that will give you just the portion of an IP address specified by a CIDR range

        `192.168.1.1` is represented in binary as `11000000 10101000 00000001 00000001`
        If we want to see if an address is in `192.168.1.1/16`, then we don't care about the last 16 bits of the address
        So we generate a bitmask that will extract the first 16 bits:  `11111111 11111111 00000000 00000000`
        Doing a bitwise AND (`&`) between a number and the bitmask will strip out everything from the rightmost two bytes
        """
        return ((1 << self.block_level) - 1) << (32 - self.block_level)

    @memoprop
    def bitstring(self):
        """Stores the bitstring for the range this object represents"""
        return self.to_number(self.octets) if self.octets else self.bitmask

    @staticmethod
    def to_number(octets):
        """Convert IP address in octet-notation to a single 32-bit number"""
        return sum(o << (8 * (3 - i)) for i, o in enumerate(octets))


class IPV4RangeList(list):
    """Creates a list of IPV4Range objects, allowing you to create a disjoint set of addresses to filter against"""
    def __init__(self, iterable):
        list.__init__(self, map(IPV4Range, iterable))

    def append(self, item):
        list.append(self, IPV4Range(item))


def set_and_load_secret_key(directory, keyfile):
    """Attempts to read and return a secret key from a file.  If that fails, writes a secret key to that file, then returns the key"""
    try:
        with open(os.path.join(directory, keyfile), 'rb') as f:
            return f.read().decode('utf-8').strip()
    except IOError:
        secret_key = get_random_secret_key()
        with open(os.path.join(directory, keyfile), 'wb') as f:
            f.write(secret_key.encode('utf-8'))
        return secret_key
