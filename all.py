# Automatically imports all the things you might want to interact with at the shell

from django.conf import settings
from django.apps import apps

from brewyastore.decorators import endpoint, memoprop, methods, route
from brewyastore.routers import API, register
from brewyastore.settings_helpers import IPV4Range, IPV4RangeList, set_and_load_secret_key
from brewyastore.urls import urlpatterns
from brewyastore.utils import simple_json_response, json_urls

from store.views import status, sessions, consoles, key, premium_purchases

# Import all models
for cfg in apps.get_app_configs():
    for model in cfg.get_models():
        locals()[model.__name__] = model