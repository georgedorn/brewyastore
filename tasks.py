import os

from invoke import Collection, Task, task, call

BASE_DIR = os.path.dirname(os.path.abspath(__file__))


def manage(ctx, dev=False, *args):
    with ctx.cd(BASE_DIR):
        cmd = '{}./manage.py '.format('BROUYASTORE_DEBUG=True ' if dev else '')
        ctx.run(cmd + ' '.join(args))

@task
def test(ctx, verbose=False):
    with ctx.cd(BASE_DIR):
        try:
            import pytest
        except ImportError:
            ctx.run('pip install -r reqs/test.txt')
        ctx.run('pytest' + (' -v' if verbose else ''))

@task
def install(ctx, dev=False):
    with ctx.cd(BASE_DIR):
        ctx.run('pip install -r reqs/{}.txt'.format('dev' if dev else 'base'))

@task
def migrate(ctx):
    with ctx.cd(BASE_DIR):
        ctx.run('./manage.py migrate')

@task
def collect(ctx, dev=False):
    with ctx.cd(BASE_DIR):
        manage(ctx, dev, 'collectstatic --noinput')

@task
def init(ctx, dev=False):
    install(ctx, dev=dev)
    migrate(ctx)
    collect(ctx, dev=dev)

@task
def shell(ctx, dev=False):
    with ctx.cd(BASE_DIR):
        try:
            import django_extensions
            manage(ctx, dev, 'shell_plus')
        except ImportError:
            manage(ctx, dev, 'shell')

@task
def clean(ctx):
    with ctx.cd(BASE_DIR):
        try:
            import django_extensions
            manage(ctx, 'clean_pyc')
        except ImportError:
            ctx.run('find . -name *.pyc -delete')

@task
def start(ctx, dev=False, external=False, *args):
    """Run the Django webserver"""
    init(ctx, dev=dev)
    with ctx.cd(BASE_DIR):
        manage(ctx, dev, 'runserver{}{}'.format('_plus' if dev else '', ' 0.0.0.0:8000' if external else ''))


ns = Collection(*filter(lambda f: isinstance(f, Task), locals().values()))
ns.configure({'run': {'pty': True}})