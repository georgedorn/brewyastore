from django.conf.urls import include, url

from brewyastore import decorators, routers, utils
from store import views


urlpatterns = [
    # url(r'^admin/', admin.site.urls),
    url(r'^', include(decorators.URLPATTERNS)),
    url(r'^api/v1/', include(routers.API.urls)),
    url(r'^api/v1', include(utils.json_urls(views.RESPONSE_MAP))),
]