# -*- coding: utf-8 -*-
import uuid

from django.http import HttpResponse, JsonResponse

from brewyastore.decorators import route


RESPONSE_MAP = {
    'console_configuration': 'console_configuration',
    'ratings': 'ratings',
    'gamers/me': 'me',
    'gamers/me/agreements': 'agreements',
    'wallet': 'wallet',
    'discover/home': 'home',
}

@route('status')
def status(request):
    return HttpResponse(status=204)

@route('api/v1/sessions', methods=['POST'])
def sessions(request):
    return JsonResponse({'token': uuid.uuid4()})

@route('api/v1/gamers/me/consoles', methods=['GET', 'POST', 'PUT'])
def consoles(request):
    return JsonResponse({})

@route('api/v1/gamers/key', methods=['POST', 'PUT'])
def key(request):
    return JsonResponse({}, status=201)

@route('api/v1/premium_purchases', methods=['GET'])
def premium_purchases(request):
    return JsonResponse({'games': []})
