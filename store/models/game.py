# -*- coding: utf-8 -*-
from django.db import models
from brewyastore.decorators import endpoint


# This is just a sample for the moment.  More actual content to come here as the store develops

@endpoint('games')
class Game(models.Model):
    title = models.CharField(max_length=256)
    latest_version = models.CharField(max_length=256, null=True)
    md5sum = models.CharField(max_length=256, null=True)
    apkFileSize = models.IntegerField(null=True)
    uuid = models.UUIDField(null=True)
